<?php

namespace tools\http;

class Response
{
    /**
     * @var array $mimeTypes Headers content type to files
     */
    private static $mimeTypes = [
        '.css' => 'text/css',
        '.js' => 'application/javascript',
        '.jpg' => 'image/jpg',
        '.png' => 'image/png',
        '.map' => 'application/json'
    ];

    /**
     * @var int $status http status code
     */
    protected $status;

    /**
     * @var array $json_ 
     */
    private $json_=null;

    /**
     * static_files
     * 
     * To return a file content at path and root, returns Response.
     * 
     * @param string $path A short path to the file "/path/to/file"
     * @param string $global_path A path root.
     * 
     * @return Response 
     */
    public function static_file(string $path, string $global_paths)
    {
        $path = $global_paths . $path;
        if (preg_match('/\.css|\.js|\.jpg|\.png|\.map$/', $path, $match)) {

            if (is_file($path)) {
                $this->content_type(static::$mimeTypes[$match[0]]);
                require $path;
                exit;
            }
        }
        if (is_file($path)) {
            require $path;
            exit;
        }
        return $this;
    }

    /**
     * redirect
     * 
     * Redirect user to other route in this host.
     * 
     * @param string $route A route path to redirect. ex "/home".    
     * 
     * @return Response 
     */
    public function redirect(string $route)
    {
        $host = $_SERVER['HTTP_HOST'];
        header("Location: http://$host$route");
        exit;
    }

    /**
     * json
     * 
     * To response a json. send a header content-type aplication/json. returns a Reponse.
     * 
     * @param array $array A array php directories as json. 
     * 
     * @return Response 
     */
    public function json(array $json)
    {
        $this->content_type(static::$mimeTypes[".map"]);
        if(!$this->json_)$this->json_=[];
        $this->json_=array_merge($this->json_,$json);
        return $this;
    }

    /**
     * content_type
     * 
     * To send a content-type to the user.
     * 
     * @param string $content A header content type
     * 
     * @return Response 
     */
    public function content_type(string $content)
    {
        header("Content-Type: " . $content);
        return $this;
    }
    /**
     * header
     * 
     * To send a multiplys headers to user. return Response.
     * 
     * @param array $header A array of strings as headers
     * 
     * @return Response
     */
    public function headers(array $headers)
    {
        foreach ($headers as $h) {
            header($h);
        }
        return $this;
    }
    /**
     * status
     * 
     * To send a  header status to user. return Response.
     * 
     * @param int $status A int as http status code
     * 
     * @return Response
     */
    public function status(int $s)
    {
        if (is_numeric($s)) {
            $this->status=$s;
            header("HTTP/1.0 $s");
        }
        return $this;
    }

    /**
     * afterController
     * 
     * Will call after controller called. 
     *
     * 
     * @return void
     */
    public function aftercontroller()
    {
        $this->json_encode();
        $this->errors();
         
    }

    /**
     * json_encode
     * 
     * Transoform json array as string json and will send to the user. 
     *
     * 
     * @return void
     */
    protected function json_encode(){
        if($this->json_){                        
            echo json_encode($this->json_);
            exit;
        }
    }

    /**
     * errors
     * 
     * to show errors view throw status. 
     *
     * 
     * @return void
     */
    protected function errors(){
        switch ($this->status) {
            case 404 | 400:                
                    view("errors/404");                
                break;
            case 500:                
                    view("errors/500");                
                break;
        }
    }
}