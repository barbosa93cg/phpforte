<?php

use tools\Autoload;
use function forte\const_vars;

$root = __DIR__.'/..';

$app = const_vars("");

$tools = __DIR__;

require $root."/vendor/autoload.php";



require $app->root."/settings/globals.php";

$includes = [
    "$tools/includes"
];

//app includes

if(isset($app->includes)) $includes = array_merge($includes,$app->includes);

tools\Autoload::inc($includes);

$libs = [
    "$tools/http",         
     app_dirs("models"),
     app_dirs("skins"),         
];

tools\Autoload::load($libs);

require "$tools/App.php";
require  app_dirs("routes");
require "$tools/Router.php";