<?php
namespace app\controllers;

use tools\http\Request;


class HomeController
{
    public function index(Request $request)
    {
        return view("index",["data"=>"Hola Mundo"]);
    }
}