<?php

use tools\http\View;

/**
 * View response.
 * 
 * We abel to show a files .tpl.php at ./views, if file is not found then send status 500.
 * 
 * @param view Name of file by root ./views.
 * @param data array data to view template. 
 * 
 * @return tools/http/Response
 */
function view(string $view,array $data = []){
    $data = array_merge($data,[
        "up_template"=>function($data){
            return "{{$data}}";
        }
    ]);
    return View::Response($view,$data);
}