<?php

namespace tools\console\build;

use tools\console\command;
use function forte\const_vars;


class build extends command
{
  public function __construct()
  {
    if(key_exists("globals",const_vars(""))){
      $this->addCommand(new app());
    }else{
      $this->addCommand(new model());
      $this->addCommand(new controller());
      $this->addCommand(new skin());
    }

    

    $this->description = "We can use it for create forte components!.";
  }

}