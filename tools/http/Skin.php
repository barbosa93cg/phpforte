<?php

namespace tools\http;

use tools\http\Request;
/**
 * We use to create  a router filters after controllers method will run.
 *  
 * We can create a customs methods as better you prefer at childs.
 */
abstract class Skin
{
    /**
     * we use it for evaluate a request
     * 
     * a Skin method.
     * 
     * @param request 
     * 
     * @return bool
     */
    public function exec(Request $request)
    {
        return true;
    }
}