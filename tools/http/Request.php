<?php

namespace tools\http;
use tools\http\blueprints\Blueprint;
/**
 * Request
 * 
 * Get params or vars or path at routing.
 */
class Request
{
    /**
     * @var array $vars A uri path vars at route.
     */
    public $vars;

     /**
     * @var array $vparams A query params at uri. for ajax implementations.
     */
    public $params;

     /**
     * @var array $path A uri rule path value at route.
     */
    public $path;

    public function __construct(array $vars,string $path,array $params)
    {
        $this->vars = $vars;
        $this->path = $path;
        $this->params = $params;
    }
}