<?php

use tools\http\Response;
/**
 * response is a global function.
 * 
 * We can use it to take easy access to response class 
 * 
 * @return Response
 */
function response(){
    return new Response();
}