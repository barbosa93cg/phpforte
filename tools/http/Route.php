<?php

namespace tools\http;

use tools\http\Request;
use tools\http\Response;
use function forte\const_vars;

/**
 * We can use this tool for create a door in the server to controller and reponse throw  user request. 
 * 
 * @api
 */
class Route
{
    public static $cache = 'forte-router';

    private static $router_get = [];
    
    private static $route_ = "";
    private static $deep = 0;
    private static $skins = [];
    

    private static function addRouter($name, $method, ConfigRoute $route)
    {
        $route->skins(self::$skins);
        
        
        const_vars([Route::$cache,$method,self::$route_.$name],$route);
        
        return $route;
        
    }

    private static function route(string $name, string $http_method, $controller)
    {
        return Route::addRouter($name, $http_method, new ConfigRoute($controller));
    }

    public static function get(string $url, $controller)
    {
        return Route::route($url, 'GET', $controller);
    }

    public static function post(string $url, $controller)
    {
        return Route::route($url, 'POST', $controller);
    }

    
    public static function extends(string $url,$callback,array $skins_classes=[]){
        static::$route_ .= $url;
        static::$skins = array_merge(static::$skins,$skins_classes);
        static::$deep++;
        $callback();
        static::$deep--;
        if(static::$deep == 0){
            static::$route_="";
            static::$skins = [];
        }
    }
}




