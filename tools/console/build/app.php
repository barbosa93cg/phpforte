<?php

namespace tools\console\build;

use tools\console\command;

/**
 * app
 * 
 * Command to create a new app web files 
 */
class app extends command
{
    public function __construct()
    {
        $this->arguments=["name"=>"A name to new app."];
    }

    public function exec($args){
        
        if($this->has_agrumnets($args,$var)){
            $root = __DIR__."/../../..";
            $this->recurse_copy("$root/tools/forte/app","$root/".$var["name"]);

            $package = file_get_contents("$root/package.json");
            $json = json_decode($package);

            $scripts = &$json->{"scripts"};

            $scripts->{"forte-".$var["name"]."-build"}="cd ./".$var["name"]." & webpack ";
            $scripts->{"forte-".$var["name"]."-watch"}="cd ./".$var["name"]." & webpack --watch --progress";
            

            file_put_contents("$root/package.json",json_encode($json));
        }
    }


    private function recurse_copy($src,$dst) { 
        $dir = opendir($src); 
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) { 
            if (( $file != '.' ) && ( $file != '..' )) { 
                if ( is_dir($src . '/' . $file) ) { 
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file); 
                } 
                else { 
                    copy($src . '/' . $file,$dst . '/' . $file); 
                } 
            } 
        } 
        closedir($dir); 
    } 


}