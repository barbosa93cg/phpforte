<?php

namespace tools\http;


use Dwoo;

use Twig_Loader_Filesystem;
use Twig_Environment;


/**
 * We can takes the ./views files to show to user.
 * 
 *  
 */

class View
{
    /**
     * Response and compile views
     * 
     * @param view
     * @param data
     * 
     * @return Response
     */
    public static function Response(string $view, array $data)
    {

        $tpl = static::twig($view, $data);
        if ($tpl) {
            echo $tpl;
            return response()->status(200);        
        }
        return response()->status(500);
    }

    /**
     * Compiler tpl.php views with dwooCore
     * 
     * @param view string
     * @param data array
     * 
     * @return string
     * 
     * @decapretated
     */
    private static function dwooCore($view, array $data)
    {
        $path = app_dirs('views')."/". $view . ".tpl.php";
        if (is_file($path)) {
            $dwoo = new Dwoo\Core();
            $tpl = new Dwoo\Template\File($path);
            $ddata = new Dwoo\Data();
            foreach ($data as $key => $value) {
                $ddata->assign($key, $value);
            }
            return $dwoo->get($tpl, $ddata);
        }
    
    }

    private static function twig($view,$data){        
        $loader = new Twig_Loader_Filesystem(app_dirs('views'));
        $twig = new Twig_Environment($loader, array(
            'cache' => "../settings/cache",
        ));
        return $twig->render($view . ".view",$data);
    }
}

