<?php

namespace tools\console;

/**
 * Create a class file on console commands
 */
abstract class factory_class extends command
{
    /**
     * @var string $root The class file root.
     */
    protected $root = "";
    /**
     * @var string $namesplace_root The class namesapce root.
     */
    protected $namesplace_root = "";
    /**
     * @var array $extends Class extends strings.
     */
    protected $extends = [];
    /**
     * @var array $using namespace class string to using in the file.
     */
    protected $using = [];
    /**
     * @var string $content The class content block.
     */
    protected $content = "";

    /**
     * Create a class 
     * @param string $name class name
     * 
     * @return void
     * 
     */
    protected function factory(string $name)
    {

        $br = "\n";
        $a_name = explode("/", $name);
        end($a_name);
        $name_l = key($a_name);
        $namespace = "";
        $n_dir = "";
        foreach ($a_name as $k => $v) {
            if ($k != $name_l) {
                $namespace .= "\\" . $v;
                $n_dir .= "/" . $v;
            }
        }
        $file = "<?php$br$br" . "namespace " . $this->namesplace_root . $namespace . ";" . $br;
        foreach ($this->using as $vals) {
            $file .= $br . "use $vals;";
        }

        $file .= "$br$br" . "class " . $a_name[$name_l];
        $i = 0;
        foreach ($this->extends as $ext) {
            if ($i == 0) {
                $file .= " extends ";
            }
            $i++;
            $file .= $ext;
            if (\array_key_exists($i, $this->extends)) {
                $file .= ",";
            }
        }

        $file .= " {" . $br . $br . $this->content . $br;

        $file .= "$br$br}";

        $dir = $this->root;

        if (!is_dir($dir . "/" . $n_dir)) mkdir($dir . "/" . $n_dir);
        file_put_contents($dir . "/" . $name . ".php", $file);
    }

}