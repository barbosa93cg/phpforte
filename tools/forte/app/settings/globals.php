<?php

use function forte\const_vars;

const_vars("settings", [
    /**
     * @gloabal app_database 
     * 
     **/
    "database" => include __DIR__ . "/database.php",
    /**
     * @gloabal app_dis 
     * */
    "dirs" => include __DIR__ . "/dirs.php",
    /**
     * @gloabal app_host
     * */
    "host" => include __DIR__ . "/host.php",
    /**
     * @gloabal includes
     * */
    "includes" => include __DIR__ . "/includes.php",

]);


