<?php

namespace forte\blueprints;

/**
 * Blueprint
 * 
 * Tranform a array directory in Blueprint properyties.
 */

class Blueprint {
    /**
     * Contructor
     * 
     * takes a array to transform as Blueprint
     * 
     * @param array $array Array to transform.
     * 
     */
    public function __construct(array $array = [])
    {
        foreach($array as $prop=>$value){
            $this->{$prop}=$value;
        }
    }
}