<?php

use tools\Autoload;

$root = __DIR__.'/..';

$app = $GLOBALS["forte_app"];

$tools = __DIR__;

require $root."/vendor/autoload.php";

require "$tools/autoloadlib.php";

require $app["root"]."/settings/globals.php";

$includes = [
    "$tools/includes"
];

//app includes

if(key_exists("includes",$app)) $includes = array_merge($includes,$app["includes"]);

tools\Autoload::inc($includes);

$libs = [
    "$tools/http",         
     app_dirs("models"),
     app_dirs("skins"),         
];

tools\Autoload::load($libs);

require "$tools/App.php";
require  app_dirs("routes");
require "$tools/Router.php";
