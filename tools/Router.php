<?php

namespace router;

/**
 * this is Aplication Router
 */


use tools\http\Route;
use tools\http\Request;
use function forte\const_vars;

/** @var string */
$method = $_SERVER['REQUEST_METHOD'];
$url = $_SERVER['REQUEST_URI'];

$params = [];

if(array_key_exists("QUERY_STRING",$_SERVER)){
    $a_query =explode("&",$_SERVER["QUERY_STRING"]);
    foreach($a_query as $d){
        $data = explode("=",$d);
        $paras[$data[0]]=$data[1];
    }
}



$routes = const_vars([Route::$cache,$method]);


$stats = [
    'finder' => false,    
];

if(preg_match("/(.+)\/$/",$url,$mtch)){
    $url = $mtch[1];
}

$a_url = explode("/", $url);
end($a_url);
$url_l = (int)key($a_url);

$response = null;

foreach ($routes as $rurl => &$config) {
    
    $a_urlpath = explode("/<path>", $rurl);

    $a_rurl = explode("/", $a_urlpath[0]);
    end($a_rurl);
    $rurl_l = (int)key($a_rurl);
    if ($rurl_l == $url_l || (array_key_exists(1,$a_urlpath) && $rurl_l <= $url_l) ) {
        $vars = [];
        $path = "";
        foreach ($a_rurl as $key => $rurl_part) {
            if(preg_match('/\A{(.+)}\z/', $rurl_part, $matches)){
                $var = $matches[1];
                $vars[$var] = $a_url[$key];                
            }else if($rurl_part != $a_url[$key]){
                $stats["finder"] = false;
                break;
            }
            $stats["finder"] = true;
        }
        if( $stats["finder"]){            
            if (array_key_exists(1, $a_urlpath) && array_key_exists($rurl_l+1, $a_url)) {                
                for($i = $rurl_l+1; $i <= $url_l; $i++){
                    $path = $path."/".$a_url[$i];                    
                }
            }
            $request = new Request($vars,$path,$params);
            if($config->eval($request)){
                $response=$config->callController($request);
                if(is_a($response,"tools\http\Response")){
                    $response->aftercontroller();
                }             
                exit;
            }
            break;

        }
    }

}

header("HTTP/1.0 404");
view("errors/404");