<?php
$tools = __DIR__."/../tools";
require "$tools/autoloadlib.php";

use tools\Autoload;

Autoload::inc([
    "$tools/forte/globals.php"
]);


forte\const_vars("root",__DIR__);
forte\const_vars("namespace","app");


Autoload::load([
    "$tools/libs.php"
]);
