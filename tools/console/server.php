<?php

namespace tools\console;

class server extends command{

    public function __construct(){
        $this->description = "To run server aplication web.";
        $this->arguments = [
            "-v"=>"start host on browser."
        ];
    }

    protected function exec($args){
        $root="";

        if(preg_match("/\.\/(.+)forte$/",$args[0],$match)){
            $root=$match[1];
        } 
        $host = app_host("host").":".app_host("port");

        if($this->has_agrumnets($args,$var))
        {
            $this->print("runing http://$host\n","blue","green");
            shell_exec("php -S ".$host.
            " ./".$root."index.php | start http://\n".
            $host."");
        }else{
            $this->print("runing http://$host\n","blue","green");
            shell_exec("php -S ".$host." ./".$root."index.php ");
        }
    }
    
}