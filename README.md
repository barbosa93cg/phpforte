# phpForte - App

- ### stable version

Creat a great app web, how many you wants and wish you want in one pack!.

- ## install
    - Composer packages `$ composer update`    
    - Node modules `$ npm install`
        - Compiler assets `$ npm run forte-app-build`
    - Run server `$php ./app/forte server`

- ## documentations
    - Run Api docs `$ php forte docs`
        - you should download [graphviz]("http://www.graphviz.org/download/")
        and include `/bin` to your path!
        - To compile the docs `$ php forte docs -c`

- ## Make a new forte application
    - Put in root files: `$ php forte build app MyApp`
    - Automaticly forte make a dir on `root` with all code of your new app.
    - you can compiler asset by `$ npm forte-MyApp-build`
    - Settings the host configuation at `MyApp/settings/host.php`
    - Settings the database configuation at `MyApp/settings/database.php`
    - Performance your own apps directories at `MyApp/settings/dirs.php`
    - Run server `$php ./MyApp/forte server`

- ## for developers
    - if you work at app realese, work at `tools/forte/app`.
    - after work realese do `php forte build app app`