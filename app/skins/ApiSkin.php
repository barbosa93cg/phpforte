<?php

namespace app\skins;

use tools\http\Skin;
use tools\http\Request;


class ApiSkin extends Skin{
    public function exec(Request $request)
    {
        if($request->vars["key"] == "api-key"){
            return true;
        }
        return false;
    }
    
    public function custom(Request $request,$data){
        if($request->vars["key"] == $data){
            return true;
        }
        return false;
    }
}