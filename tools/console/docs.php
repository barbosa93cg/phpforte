<?php

namespace tools\console;

class docs extends command{

    public function __construct(){
        $this->description = "To create documentations with phpdoc.";        
        $this->arguments = [
            "-c" => "compiler docs"
        ];
    }

    protected function exec($args){
        if($this->has_agrumnets($args,$var)){
            switch($var["-c"]){
                case "-c":                    
                    system('.\\vendor\\bin\\phpdoc -d ./tools -t ./tools/forte/docs/api');
                    break;
                default:
                    $this->print($var["-c"]."is not a command for docs.","white","red");
            }
        }else              
        system("cd ./tools/forte/docs/api & php -S localhost:8080 | start http://localhost:8080");
        
    }
    
}