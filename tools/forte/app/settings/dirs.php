<?php

/**
 * Dirs
 * 
 * Vars that has a path of files require to build the aplication.
 * 
 */

$root = __DIR__."/..";

return [
    /**
     * controllers
     */
    'controllers'=> $root."/controllers",
    /**
     * public
     */
    'public'=> $root."/public",
    /**
     * views
     */
    'views'=> $root."/views",
    /**
     * models
     */
    'models'=> $root."/models",
    /**
     * skins
     */
    'skins'=> $root."/skins",
    /**
     * routes
     */
    'routes'=> $root."/routes.php",    
];