<?php

namespace tools\console;

use function forte\const_vars;

$app = const_vars("");

//globals 
if(isset($app->globals))
    require $app->globals;
else {
    require $app->root."/settings/globals.php";
    include __DIR__."/../includes/Settings.php";
}






use tools\console\build\build;


class forte extends command{
    public function __construct(){
        if(isset(const_vars("")->globals)){
            $this->addCommand(new docs()); 
        }else{
            $this->addCommand(new server());
        }
        $this->addCommand(new build());             
        $this->addCommand(new help()); 
        
        $this->title = "--FORTE COMMANDS HELP--\n";
    }    
}