<?php

namespace tools\console;

abstract class command
{
    protected $commands = [];
    protected $parent = null;
    protected $arg_key = 0;

    protected $title = "";
    protected $description = "";
    protected $arguments = [];

   

    protected function addCommand(command $command)
    {
        $command->parent = $this;
        if (preg_match('/\\\(\w+)$/', get_class($command),$match)){
            $this->commands[$match[1]] = &$command;
        }        
    }

    

    public function run(array &$args)
    {
        
        if ($this->parent) $this->arg_key = $this->parent->arg_key + 1;           
        if (array_key_exists($this->arg_key, $args)) {
            if (preg_match('/' . $args[$this->arg_key] . '$/', get_class($this))) {
                $this->exec($args);
                foreach ($this->commands as $command) {
                    $command->run($args);
                }
            }
        }
    }


    protected function print($trsing,$forground=null,$backgound=null){
        $colors = new Colors();
        echo $colors->getColoredString($trsing,$forground,$backgound);
    }

    protected function help()
    {
        $tabs = "";
        for ($i = 0; $i < $this->arg_key && $this->arg_key > 1; $i += 2) {
            $tabs .= "\t";
        }
        $args = "";
        $args_name = "";
        foreach ($this->arguments as $key => $val) {
            $args .= "\n$tabs\t[" . $key . "] >$val";
            $args_name .= " [" . $key . "]";
        }

        if (preg_match('/\\\(\w+)\z/', get_class($this), $match) && $this->title == "") {
            $this->title = $match[1] . $args_name;
        }
        echo "\n$tabs" . $this->title;
        if ($this->description != "") echo "\n$tabs\t>" . $this->description;
        echo $args;
        foreach ($this->commands as $command) {
            $command->help();
        }

    }

    protected function exec($args)
    {

    }

    protected function has_agrumnets($args, &$arguments)
    {
        $arguments = [];
        $i = 0;
        foreach ($this->arguments as $arg => $des) {
            if (array_key_exists($this->arg_key + $i + 1, $args)) {
                $arguments[$arg] = $args[$this->arg_key + $i + 1];
            } else return false;
            $i++;
        }
        return true;
    }

}



