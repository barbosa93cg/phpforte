<?php

namespace tools\http;

use tools\http\Request;
/**
 * Config Route 
 */
class ConfigRoute
{
    private $class;
    private $method;
    private $controller_namespace = "controllers\\";
    private $controller = null;
    private $skins = [];   

    public function __construct($controller)
    {
        if (is_string($controller)) {
            $a_c = explode("@", $controller);
            $this->class = $a_c[0];
            $this->method = $a_c[1];
        } else
            if (is_callable($controller)) {
            $this->controller = $controller;
        }

    }

    public function callController(Request $request)
    {
        
        if (!$this->controller) {
            require $GLOBALS["app_dis"]["controllers"] . "/" . $this->class . ".php";
            $classC = $this->controller_namespace . $this->class;
            $c = new $classC;
            return $c->{$this->method}($request);
        } else {
            $c = $this->controller;
            return $c($request);
        }

    }

    public function skins(array $skins){
        $this->skins = array_merge($this->skins,$skins);
    }

    public function eval(Request $request){
        foreach($this->skins as $cskins){
            $class = "skins\\".$cskins;
            if(class_exists($class)){
                $skin = new $class;
                if(!$skin->exec($request)){
                    return false;
                }
            }else{
                $a_class = explode(":",$class);
                if(class_exists($a_class[0])){
                    $a_a_class = explode("(",$a_class[1]);
                    $param;
                    if(array_key_exists(1,$a_a_class)){
                        $param = str_replace(")","",$a_a_class[1]);
                    }
                    $skin = new $a_class[0];
                    if(!$skin->{$a_a_class[0]}($request,$param)){
                        return false;
                    }                    
                }
            }
        }
        return true;
    }
}
