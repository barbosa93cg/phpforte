<?php

/**
 * forte_app
 * 
 * Get a global forte_app key value. 
 * 
 * @param mixed $key forte_app key
 * @return mixed 
 */
function forte_app($key){
    return $GLOBALS["forte_app"][$key];
}

/**
 * forte_app
 * 
 * Get a global forte_app values. 
 * 
 * @return array 
 */
function forte_app_full(){
    return $GLOBALS["forte_app"];
}

/**
 * forte_app
 * 
 * Get a global  forte_app settings key value. 
 * 
 * @param mixed $key forte_app settings key
 * @return array 
 */
function app_settings($key){
    return forte_app("settings")[$key];
}

/**
 * forte_app
 * 
 * Get a global  settings dirs key value. 
 * 
 * @param mixed $key settings dirs key
 * @return mixed 
 */
function app_dirs($key){
    return app_settings("dirs")[$key];
}

/**
 * forte_app
 * 
 * Get a global  settings host key value. 
 * 
 * @param mixed $key settings host key
 * @return mixed 
 */
function app_host($key){
    return app_settings("host")[$key];
}

/**
 * forte_app
 * 
 * Get a global settings database key value. 
 * 
 * @param mixed $key settings database key
 * @return mixed 
 */
function app_database($key){
    return app_settings("database")[$key];
}


