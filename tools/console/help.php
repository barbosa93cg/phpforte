<?php

namespace tools\console;

class help extends command{
    

    public function __construct()
    {
        $this->description ="Show all forte commands help.";
        $this->arguments = [
            "command"=>"Command to help.(optional)",
            "..."=>"childs comand.(optional)"
        ];
    }

    protected function exec($args){
        $cursor = $this->parent;
        for($i=$this->arg_key+1;\key_exists($i,$args);$i++){
            if($cursor){
                if(\key_exists($args[$i],$cursor->commands)){                    
                    $cursor = $cursor->commands[$args[$i]];                 
                }else{
                    $cursor = null;
                    $this->print("Command '".$args[$i]."' not found","white","red");            
                    break;
                }
            }
        }

        if($cursor)$cursor->help();

    }

    
}