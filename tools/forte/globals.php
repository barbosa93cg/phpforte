<?php
namespace forte;

use tools\Autoload;

Autoload::app(__DIR__."/blueprints");

use forte\blueprints\Blueprint;





function const_vars($index,$mix=null){
    
    //global name
    $global_name = "FORTE_APP";


    if(!array_key_exists($global_name,$GLOBALS))$GLOBALS[$global_name]= new Blueprint();
    $a_index = $index;
    if(is_string($index))$a_index = explode("/",$index);
    $cursor = &$GLOBALS[$global_name];
    $last="";
    
    $cursor = &_vars(0,$cursor,$a_index,$last);

    if(is_a($cursor,Blueprint::class)){
        if($mix){
            $cursor->{$last} = $mix;
            $cursor=&$cursor->{$last};
        }
    }else
    if(is_array($cursor)){
        if($mix){
            $cursor[$last] = $mix;
            $cursor=&$cursor[$last];
        }
    }
    return $cursor;
        
}



function &_vars($x=0,&$cursor,$a_index,&$last){
    if(is_a($cursor,Blueprint::class)){

        if(isset($cursor->{$a_index[$x]})){            
            $cursor=&$cursor->{$a_index[$x]};                               
        }            
        else{
            if(key_exists($x+1,$a_index))
            {
                $cursor->{$a_index[$x]}=new Blueprint();
                $cursor=&$cursor->{$a_index[$x]};
            }else{
                $last = $a_index[$x];
            }           
        }
        
    }else
    if(is_array($cursor)){
        if(key_exists($a_index[$x],$cursor)){            
            $cursor=&$cursor[$a_index[$x]];                               
        }            
        else{
            if(key_exists($x+1,$a_index))
            {
                $cursor[$a_index[$x]]=[];
                $cursor=&$cursor[$a_index[$x]];
            }else{
                $last = $a_index[$x];
                
            }           
        }
    }

    if(key_exists($x+1,$a_index))return _vars($x+1,$cursor,$a_index,$last);
    return $cursor;      
}
