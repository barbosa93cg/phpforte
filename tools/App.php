<?php

use tools\http\Request;
use tools\http\Route;
use function forte\const_vars;
$root = __DIR__.'/..'; 
$uri = $_SERVER['REQUEST_URI'];
$file = app_dirs("public")."/".$uri;

const_vars(Route::$cache,[    
    'GET'=>[],
    'POST'=>[],
    'PUT'=>[]
]);

//public static files
response()->static_file($uri,app_dirs("public"));


