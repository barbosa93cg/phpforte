<?php

namespace tools\console\build;


use tools\console\factory_class;

class model extends factory_class
{
    public function __construct()
    {
        $this->description = "To create a new model.";
        $this->arguments = [
            "name" => "Is the new file/class Model name."
        ];
    }

    protected function exec($args)
    {
        if ($this->has_agrumnets($args, $vars)) {

            $this->root = app_dirs("models");
            $this->namesplace_root = "models";
            $this->using = ["tools\\http\\Model"];
            $this->extends = ["Model"];

            $this->factory($vars["name"]);

        } else {
            echo "Model need a name!";
        }

    }


}
