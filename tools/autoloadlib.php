<?php

namespace tools;

class Autoload
{
    public static function app(string $path)
    {
        self::loadAll($path);
    }

    public static function load(array $libs,$do=null){
        foreach($libs as $file){            
            if(is_dir($file)){
                self::loadAll($file,$do);
            }
            else require $file;
        }
    }

    public static function inc(array $libs){
        self::load($libs,function($file){
            include $file;
        });
    }

    private static function loadAll(string $path,$do=null)
    {
        $gestor_dir = opendir($path);
        while (false !== ($file_name = readdir($gestor_dir))) {
            if (preg_match('/.php$/', $file_name, $matches)) {
                if(!$do)require $path . "/" . $file_name;
                else if(is_callable($do))$do($path . "/" . $file_name);            
            }
            if (is_dir($path . "/" . $file_name) && $file_name != ".." && $file_name != ".") {
               self::loadAll($path . "/" . $file_name,$do);
            }
        }
    }
}


