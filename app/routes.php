<?php
namespace app\routes;

/**
 * Create a routes for web site!
 * 
 */



use tools\http\Route;





Route::get("/", "HomeController@index");

Route::get("/{key}/user", "UserController@create")->skins(["ApiSkin"]);


Route::extends("/api/{key}", function () {
    Route::get("", function ($request) {
        return response()->json(["api" => "hello world"]);
    });
    Route::get("/wow", function ($request) {
        return response()->json(["api" => "wow"]);
    });
},["ApiSkin:custom(dok-key)"]);
