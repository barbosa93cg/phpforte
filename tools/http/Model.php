<?php

namespace tools\http;

use PDO;

abstract class Model {   

    public static function new(array $qry){
        $model = new static();
        $db = db_conection();

        $tragets = $fields = "";        

        foreach($qry as $field=>$value){
           if(preg_match("/\D*/",$value)) $tragets .= "'".$value."',";
           else $tragets .= "".$value.",";
            $fields .= "`".$field."`,";            
        }

        if(\preg_match("/(.+),\z/",$fields,$match)){
            $fields = $match[1];
        }
        if(\preg_match("/(.+),\z/",$tragets ,$match)){
            $tragets = $match[1];
        }       

        $table = static::tranform_plural_class();

        
        $sql = "INSERT INTO $table ($fields) VALUES ($tragets)";
        
        $rf=$db->query($sql,PDO::FETCH_INTO,$model);

        //terminar
        
        
    }
    
    private static function tranform_plural_class(){
        $table = "";        
        if(\preg_match("/\\\(\w+)\z/",get_called_class(),$match)){
            $table = $match[1];
        }
        $table = strtolower($table);
        $plurals = [
            "/r|a|l|\z/"=>"s"            
        ];
        foreach($plurals as $rule=>$adder){
            if(preg_match($rule,$table)){
                $table.=$adder;
            }
        }
        return $table;
    }
        
}




function db_conection(){
    $engine = app_database("engine");
    try { 
        switch($engine){
            case "mysql":
                $host = app_database("host");
                $db_name = app_database("db_name");                        
                $db = new PDO("$engine:host=$host;dbname=$db_name", app_database("user"), app_database("password"));                
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $db;
                break;
        }
    }catch (PDOException $e){
        print "¡Error!: " . $e->getMessage() . "<br/>";
        die();
    }     
}