<?php

namespace tools\console\build;


use tools\console\factory_class;

class controller extends factory_class{
    public function __construct()
    {
        $this->description = "To create a new controller.";
        $this->arguments = [
            "name"=>"A name to new controller."
        ];
    }

    protected function exec($args){
        if($this->has_agrumnets($args,$vars)){
            
            $this->namesplace_root = "controllers";
            $this->root = app_dirs("controllers");
            $this->using = ["tools\http\Request"];
            $this->content = "\tpublic function index(Request \$request){\n\n\n\t}";
            $this->factory($vars["name"]);            
            
        }
    }
}