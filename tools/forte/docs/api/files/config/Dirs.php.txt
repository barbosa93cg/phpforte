<?php

$root = __DIR__."/..";

return [
    'controllers'=> $root."/controllers",
    'public'=> $root."/public",
    'views'=> $root."/views",
    'models'=> $root."/models",
    'skins'=> $root."/skins",    
];
