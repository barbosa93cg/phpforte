<?php

namespace tools\console\build;

use tools\console\factory_class;


class skin extends factory_class{
    public function __construct()
    {
        $this->description = "To build a skins filters.";
        $this->arguments=[
            "name"=>"skin name",            
        ];        
    }

    protected function exec($args){
        if($this->has_agrumnets($args,$name)){
            $this->root = app_dirs("skins");
            $this->namesplace_root = "skins";
            $this->_extends();
            if($this->has_agrumnets($args,$ext)){
                $this->extends = [$ext["extends"]];
                $this->using = [$ext["namespace"]];
            }else{
                $this->using = ["tools\http\Skin"];
                $this->extends = ["Skin"];
            }
            $this->factory($name["name"]);
        }
    }

    private function _extends(){
        $this->arguments = array_merge($this->arguments,[
            "extends"=>"skin class extend",
            "namespace"=>"namespace extend",
        ]);
    }

    protected function help(){
        $this->_extends();
        parent::help();
    }
}